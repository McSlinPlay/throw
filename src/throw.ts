import fs from "fs";
import path, {dirname} from "path";
import {fileURLToPath} from "url";

const __dirname = dirname(fileURLToPath(import.meta.url));

let objects = null

function loadObjects() {
    objects = []
    let path1 = path.join(__dirname, '../throws.txt')
    fs.readFileSync(path1)
      .toString()
      .split('\n')
      .map(s => s.trim())
      .filter(s => s !== "")
      .forEach((line) => {
          objects.push(line)
      })
}

export default function getObjects() {
    if (objects === null) {
        loadObjects()
    }
    // determine if 1,2 or 3 objects
    // 1 is the most common and 3 should be very rare
    const weight = {
        1: 0.6,
        2: 0.3,
        3: 0.1
    }
    let sum = 0, r = Math.random(), num;
    for (let i in weight) {
        sum += weight[i];
        if (r <= sum) {
            num = i;
            break;
        }
    }

    const o = []
    for (let i = 0; i < num; i++) {
        o.push(objects[Math.floor(Math.random() * objects.length)])
    }
    return o
}
